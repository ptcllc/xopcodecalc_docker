#!/bin/bash

docker build --tag ptcllc/xopthingy .

echo """
docker run --rm \\
    --name "XopThing"  \\
    -v="$HOME/.Xauthority:/root/.Xauthority:rw" \\
    --net=host \\
    --env DISPLAY="$DISPLAY" \\
    ptcllc/xopthingy
"""

# syntax=docker/dockerfile:1
FROM debian
RUN apt-get -y update
RUN apt-get install --assume-yes git build-essential qtbase5-dev qttools5-dev-tools
RUN git clone --recursive https://github.com/horsicq/XOpcodeCalc.git

RUN cd XOpcodeCalc && bash -x build_dpkg.sh && dpkg -i release/*.deb

CMD ["xocalc"]

